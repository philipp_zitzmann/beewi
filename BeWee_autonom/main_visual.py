import edge_detection, bt_comm as auto, numpy, cv2, config, thread, Queue, traceback
import matplotlib.pyplot as plt
from time import sleep
from threading import Event
#bt_comm.connectToCar()
#bt_comm.testCommands()
#bt_comm.disconnect()
cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FPS, 20)
newValue = Event()
drivingDone = Event()
freshness = Queue.LifoQueue()
distance = Queue.LifoQueue()
currentImage = Queue.LifoQueue()
def drawLines(image, freshness, distance):
	try:
		linedImage = edge_detection.pipeline(image, freshness, distance, drivingDone)
		if config.only_image_processing == False:
			newValue.set()
		return linedImage
	except Exception, e:
		print 'Exception closing!!!'
		traceback.print_exc()
		cap.release()
		return image

def take_images():
    while cap.isOpened():
        ret,image = cap.read()
        if currentImage.qsize() >= 100:
                #prevent from queue getting too long
                currentImage.get()
        currentImage.put(image)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    print 'cap closed'
    cap.release()
    cv2.destroyAllWindows()
    if config.only_image_processing == False:
            auto.disconnect()        
def run_detection(freshness, distance):
    while cap.isOpened():
        image = currentImage.get()
        lined = drawLines(image, freshness, distance)
        cv2.imshow('Stream', lined)
        #plt.figure()
        #plt.imshow(lined)
        #plt.show()
def drive(freshness, distance):
    drivingDone.set()
    while (cap.isOpened()):
        newValue.wait()
        abstand = distance.get()
        print('**********aktueller Abstand:' + str(abstand) + " **********")
        if not abstand:
            print('distance Null!!!')  
        elif abstand <= 40 and abstand >= - 40:
            auto.forward()
            print('******forwarding******')
        elif abstand < -40 and abstand >= -100:
            print('******left******')
            auto.slightlyLeft()
        elif abstand > 40 and abstand <= 100:
            print('******Right******')
            auto.slightlyRight()
        elif abstand > 100:
            print('******Hard Right******')
            auto.hardRight()
        elif abstand < -100:
            print('******Hard Left******')
            auto.hardLeft()       
        newValue.clear()
        drivingDone.set()
        sleep(0.2)

freshness.put(-1)
distance.put(0)
if config.only_image_processing == False:
	print 'connecting'
	auto.connect()
thread.start_new_thread(take_images,())
thread.start_new_thread(run_detection,(freshness, distance))
if config.only_image_processing == False:
    thread.start_new_thread(drive,(freshness, distance))
