import cv2
import numpy as np
import config
##########################################Image Manipulation Methods#######################

# Region can be manioulated in Config -> cut_top
def region_of_interest(img):
    # make top Area with height = cut_top black
    #defining a blank mask to start with
    imshape = img.shape
    mask = np.zeros_like(img)
    #fill the bottom of the mask with the bottom of the picture
    mask[config.cut_top : imshape[0],
         0:imshape[1],] = img[config.cut_top: imshape[0]
                                  , 0: imshape[1]]
    return mask

# calculates optimun threshold Values
def auto_canny(image, sigma=0.33):
    # compute the median of the single channel pixel intensities
    imshape = image.shape
    mask = image[config.cut_top: imshape[0] , 0: imshape[1]]
    v = np.median(np.array(mask))
    # apply automatic Canny edge detection using the computed median
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))
    edged = cv2.Canny(image, lower, upper)
    return edged
#blur the image
def blur_image(image):
    if config.use_median_blur:
        filtered = cv2.medianBlur(image,7)
    if config.use_gaussian_blur:
        filtered = cv2.GaussianBlur(filtered, (7,7), 0)
    if config.use_morph:
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(7,7))
        filtered =cv2.morphologyEx(filtered,cv2.MORPH_OPEN,kernel)   
        filtered =cv2.morphologyEx(filtered,cv2.MORPH_CLOSE,kernel)
    return filtered
        
# prefilters the image after selected colors
#colors can be selected in Config
def prefilter_image(image):
    #blur median and gaussian
    masked_image = select_colors(image)
    #got the mask now blur it
    k_size = config.kernel_size_blur
    if config.blur_type == config.Blur.gaussian :
        blur = cv2.GaussianBlur(msked_image,(k_size,k_size),0)
    elif config.blur_type == config.Blur.median :
        blur = cv2.medianBlur(masked_image,1)
    elif config.blur_type == cofig.Blur.blur:
        blur = cv2.blur(masked_image,(k_size,k_size))
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(5,5))
    maskOpen=cv2.morphologyEx(blur,cv2.MORPH_OPEN,kernel)   
    maskClose=cv2.morphologyEx(maskOpen,cv2.MORPH_CLOSE,kernel)
    return maskClose

def select_colors(image):    
    img_hsv=cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    hsv_copy = np.copy(image)
    hsv_copy = cv2.cvtColor(hsv_copy, cv2.COLOR_RGB2GRAY)
    mask = None
    for color in config.colors :
        if color == Color.red :
            mask0 = cv2.inRange(img_hsv, config.lower_red, config.upper_red)
            mask1 = cv2.inRange(img_hsv, config.lower_red1, config.upper_red1)
            mask2 = cv2.bitwise_or(mask0, mask1)
            if mask == None:
                mask = np.zeros_like(mask2)
            mask = cv2.bitwise_or(mask, mask2)
            print('red selected')
        elif color == Color.white :
            mask0 = cv2.inRange(img_hsv, config.lower_white, config.upper_white)
            if mask == None:
                mask = np.zeros_like(mask0)
            mask = cv2.bitwise_or(mask, mask0)
            print('white selected')
        elif color == Color.blue :
            print('blue selected')
            mask0 = cv2.inRange(img_hsv, config.lower_blue, config.upper_blue)
            if mask == None:
                mask = np.zeros_like(mask0)
            mask = cv2.bitwise_or(mask, mask0)
    return mask
