import math
import config
import itertools
import cv2
import numpy as np

# Given intersections, find the grid where most intersections occur and treat as vanishing point
def find_vanishing_point(img, grid_size, line_intersection_dict):
    # Image dimensions
    image_height = img.shape[0]
    image_width = img.shape[1]

    # Grid dimensions
    grid_rows = ((image_height) // grid_size) + 1
    grid_columns = ((image_width/2) // grid_size) + 1

    # Current cell with most intersection points
    max_intersections = 0
    best_cell = (0.0, 0.0)
    best_cell_lines_left = []
    best_cell_lines_right = []

    for i, j in itertools.product(range(grid_rows), range(grid_columns)):
        cell_left = i * grid_size
        cell_right = (i + 1) * grid_size
        cell_bottom = j * grid_size
        cell_top = (j + 1) * grid_size
        if config.show_grid:
            cv2.rectangle(img, (cell_left, cell_bottom), (cell_right, cell_top), (0, 0, 255), 1)
        

        current_intersections = 0  # Number of intersections in the current cell
        current_cell_lines_left = []
        current_cell_lines_right = []
        for key,value in line_intersection_dict.iteritems():
            x = value[0]
            y = value[1]
            if cell_left < x < cell_right and cell_bottom < y < cell_top:
                current_intersections += 1
                #add the two lines
                current_cell_lines_left.extend([key[0:4]])
                current_cell_lines_right.extend([key[4:8]])

        # Current cell has more intersections that previous cell (better)
        if current_intersections > max_intersections:
            max_intersections = current_intersections
            best_cell = ((cell_left + cell_right) / 2, (cell_bottom + cell_top) / 2)
            best_cell_lines_left = set(current_cell_lines_left)
            best_cell_lines_right = set(current_cell_lines_right)
            print("Best Cell:", best_cell)
            print("intersections", max_intersections)

    if best_cell[0] != None and best_cell[1] != None:
        rx1 = int(best_cell[0] - grid_size / 2)
        ry1 = int(best_cell[1] - grid_size / 2)
        rx2 = int(best_cell[0] + grid_size / 2)
        ry2 = int(best_cell[1] + grid_size / 2)
        if config.show_grid:
            cv2.rectangle(img, (rx1, ry1), (rx2, ry2), (0, 255, 0), 1)
    return [best_cell_lines_left, best_cell_lines_right]

# Find intersection point of two lines (not segments!)
def line_intersection(line1, line2):
    xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])
    
    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(xdiff, ydiff)
    if div == 0:
       return None

    d = (det(*line1), det(*line2))
    x = det(d, xdiff) / div
    y = det(d, ydiff) / div
    return x, y


# Find intersections between multiple lines (not line segments!)
def find_intersections(left_lines, right_lines):
    lines_with_intersection = {}
    for line in left_lines :
        for r_line in right_lines:
            intersection = line_intersection(line, r_line)
            if intersection:
                key1 = tuple(line[0] + line[1])
                key2  = tuple(r_line[0] + r_line[1])
                key = key1 +key2
                new_inters = {key : intersection}
                lines_with_intersection.update(new_inters) 
    return lines_with_intersection
