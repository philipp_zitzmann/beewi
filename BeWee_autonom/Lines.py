
import Point
from Line import Line
import numpy as np

class Lines:
    
    def __init__(self):
        self.linesList = []
        self.pointsArray = []
        self.xVals = []
        self.yVals = []


    def addLine(self, line):
        self.linesList.append([(line.start.x, line.start.y), (line.end.x , line.end.y)])
        self.pointsArray.append((line.start.x, line.start.y))
        self.pointsArray.append((line.end.x , line.end.y))
        self.xVals.append(line.start.x)
        self.xVals.append(line.end.x)      
        self.yVals.append(line.start.y)
        self.yVals.append(line.end.y)
    def getArray(self):
        return np.asarray(self.pointsArray)
    def getList(self):
        return self.linesList
    def getXVals(self):
        return self.xVals
    def getYVals(self):
        return self.yVals
        
