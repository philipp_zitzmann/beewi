
from Point import Point
from Line import Line
from Lines import Lines
import numpy as np

class Main:
    start = Point(1,2)
    end = Point(2,3)
    line = Line(start, end)
    lines = Lines()
    lines.addLine(line)
    print lines.xVals
