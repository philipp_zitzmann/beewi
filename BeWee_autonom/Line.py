
from Point import Point
import numpy as np

class Line:
    def __init__(self, *args, **kwargs):
        if len(args) == 2:
            self.start = args[0]
            self.end = args[1]
        else:
            line = args[0]
            startPoint = Point(line[0], line[1])
            self.start = startPoint
            endPoint = Point(line[2],line[3])
            self.end = endPoint
