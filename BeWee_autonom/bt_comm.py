import bluetooth
import config
from time import sleep
socket = None
wheel_direction = 0
#Method for connecting to BeWee car mac adress in Config
#For connecting to different car use lookupNearbyDevices to get the address
def connect():
  global socket
  port = 1
  socket=bluetooth.BluetoothSocket( bluetooth.RFCOMM )
  socket.connect((config.Car_Mac_Address, port))
  print('Connected sucessfully')
def disconnect():
  global socket
  socket.close()
def sendMessage(message, times):
  global socket 
  for x in range(times):
    print('sending Message to: ' + config.Car_Mac_Address)
    socket.send(message[0])
  
def testCommands():
  sendMessage(config.Forward_Go,1)
  sleep(1)
  sendMessage(config.Forward_Stop,1)
  sleep(1)
  sendMessage(config.Left_Go,1)
  sleep(1)
  sendMessage(config.Left_Stop,1)
  sleep(1)
  sendMessage(config.Right_Go,1)
  sleep(1)
  sendMessage(config.Right_Stop,1)
  sleep(1)
  sendMessage(config.Backwards_Go,1)
  sleep(1)
  sendMessage(config.Backwards_Stop,1)
def slightlyRight():
    print("going right")
    sendMessage(config.Right_Go,3)
    sendMessage(config.Forward_Go,5)
    sleep(0.2)
    sendMessage(config.Right_Stop,1)
    sleep(0.2)
    sendMessage(config.Forward_Stop,1)
def slightlyLeft():
    print("going left")
    sendMessage(config.Left_Go,3)
    sendMessage(config.Forward_Go,5)
    sleep(0.2)
    sendMessage(config.Left_Stop,1)
    sleep(0.2)
    sendMessage(config.Forward_Stop,1)
def hardLeft():
  sendMessage(config.Right_Go,5)
  sendMessage(config.Backwards_Go,5)
  sleep(0.3)
  sendMessage(config.Right_Stop,1)
  sendMessage(config.Backwards_Stop,1)
  sendMessage(config.Left_Go,10)
  sendMessage(config.Forward_Go,5)
  sleep(0.4)
  sendMessage(config.Left_Stop,1)
  sendMessage(config.Forward_Stop,1)
  
def hardRight():
  sendMessage(config.Left_Go,5)
  sendMessage(config.Backwards_Go,5)
  sleep(0.3)
  sendMessage(config.Left_Stop,1)
  sendMessage(config.Backwards_Stop,1)
  sendMessage(config.Right_Go,10)
  sendMessage(config.Forward_Go,5)
  sleep(0.4)
  sendMessage(config.Right_Stop,1)
  sendMessage(config.Forward_Stop,1)
def forward():
    print("forwarding")
    sendMessage(config.Forward_Go,5)
    sleep(0.4)
    sendMessage(config.Forward_Stop,1)
def lookUpNearbyBluetoothDevices():
  nearby_devices = bluetooth.discover_devices()
  for bdaddr in nearby_devices:
    string = bluetooth.lookup_name( bdaddr ) + " [" + str(bdaddr) + "]"
    print(string)
