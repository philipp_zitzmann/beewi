import bt_comm as auto
import config
import time

def slightlyRight():
    auto.sendMessage(config.Right_Go)
    time.sleep(0.2)
    auto.sendMessage(config.Right_Stop)
def slightlyLeft():
    auto.sendMessage(config.Left_Go)
    time.sleep(0.2)
    auto.sendMessage(config.Left_Stop)
def forward():
    auto.sendMessage(config.Forward_Go)
    time.sleep(0.5)
    auto.sendMessage(config.Forward_Stop)

auto.connectToCar()
slightlyLeft()
forward()
slightlyRight()
