import numpy as np
import cv2
cap = cv2.VideoCapture(0)
while True:
    _,image = cap.read()
    cv2.imshow('Camera', image)
    if cv2.waitKey(25) & 0xFF == ord('q'):
        cv2.destroyAllWindows()
        cap.release()
        break
