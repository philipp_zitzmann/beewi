import cv2
import numpy as np
from enum import Enum

#Car Commands for Bt
Forward_Stop = '\x00'
Forward_Go = '\x01'
Backwards_Stop = '\x02'
Backwards_Go = '\x03'
Left_Stop = '\x04'
Left_Go = '\x05'
Right_Stop = '\x06'
Right_Go = '\x07'
Car_Mac_Address = '00:13:EF:D6:0A:C0'

#color Class for lane detection if not installed pip install --upgrade pip enum34
class Color(Enum):
    white = 0
    red = 1
    blue = 2
    yellow = 3
    green = 4
class Blur(Enum):
    blur = 0
    gaussian = 1
    median = 2
# show onsy the video Stream without driving
only_image_processing = True
# blur Settings
use_median_blur = True
use_gaussian_blur = False
use_morph = False
#show the Grid?
show_grid = True
# show canny Edges ?
show_canny = False
# show intersection Points ?
show_intersection_points = False
#list of filtered Colors:
colors = [Color.blue]
#top border for region of interest:
cut_top = 250
#perfilter Image? True = filter it
prefilter = False
blur_type = Blur.gaussian
kernel_size_blur = 13
###############################Masks:
########red########
# lower mask (0-10)
lower_red = np.array([0,50,50])#evtl 2.wert 70
upper_red = np.array([10,255,255])
# upper mask (170-180)
lower_red1 = np.array([170,50,50])
upper_red1 = np.array([180,255,255])
#######blue#######
lower_blue = np.array([110,50,50])
upper_blue = np.array([130,255,255])
#######yellow#######
lower_yellow = np.array([20,100,100])
upper_yellow = np.array([30,255,255])
#######green#######
lower_green = np.array([36,0,0])
upper_green = np.array([86,255,255])
#######white#######
lower_white = np.array([0,0,0])
upper_white = np.array([0,0,255])

