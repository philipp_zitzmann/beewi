import edge_detection, cv2, config
import bt_comm as auto
import matplotlib.pyplot as plt
import time
from time import sleep
avg = (0, 0, 0, 0)
cap = cv2.VideoCapture(0)
def drawLines(image):
    linedImage = edge_detection.pipeline(image)
    return linedImage
def calculateMedianDistance(image):
    abweichung = edge_detection.calculateMedianDistance(image)
    print('abweichung:')
    print(abweichung)
    return abweichung
auto.connect()

while True:
    print('Start:')
    print(time.ctime())
    global avg
    _,image = cap.read()
    abweichung = calculateMedianDistance(image)
    if abweichung is None:
        print('Abweichung none')
    else:
        print(avg)
        avg = (abweichung, avg[0], avg[1], avg[2])
        movAvg = sum(avg) / float(len(avg))
        print('Moving Average:')
        print(movAvg)
        if movAvg <= 30 and movAvg >= - 30:
            auto.forward()
            print('******forwarding******')
        elif movAvg < -30:
            print('******left******')
            auto.slightlyLeft()
        elif movAvg > 30:
            print('******right******')
            auto.slightlyRight()

        
    print('Ende:')
    print(time.ctime())

#while True:
#    _,image = cap.read()
#    lined = drawLines(image)
#    plt.figure()
#    plt.imshow(lined)
#    plt.show()
#    sleep(config.cyclic_picture)
