# -*- coding: utf-8 -*-
##########################Imports####################################
import cv2
import numpy as np
import matplotlib.pyplot as plt
import math
import config
from config import Color
import image_filter as i_filter
import vanishing_point_detection as vpd
from Lines import Lines
from Line import Line
from Point import Point


# function for Drawing the Lane lines
def draw_lines(image, lines, color=[0, 0, 255], thickness=2, make_copy=True):
    # the lines returned by cv2.HoughLinesP has the shape (-1, 1, 4)
    if make_copy:
        image = np.copy(image) # don't want to modify the original
    for line in lines:
        for x1,y1,x2,y2 in line:
            cv2.line(image, (x1, y1), (x2, y2), color, thickness)
    return image
def drawLines2(image, lines, color=[0, 0, 255], thickness=2, make_copy=True):
    # the lines returned by cv2.HoughLinesP has the shape (-1, 1, 4)
    if make_copy:
        image = np.copy(image) # don't want to modify the original
    for line in lines:
        pointStart = line[0]
        pointEnd = line[1]
        cv2.line(image, pointStart, pointEnd, color, thickness)
    return image
def det(a, b):
    return a[0] * b[1] - a[1] * b[0]
#Pipeline for calculating the median distance
def pipeline(image, freshness, distance, drivingDone):
    """
    An image processing pipeline which will output
    an image with the lane lines annotated.
    """
    # Fist step cut the top
    roi_image = i_filter.region_of_interest(image)
    #Secound Step Filter for colors or only blur the image
    if config.prefilter == True:
        filtered = i_filter.prefilter_image(roi_image)
    else:
        filtered = i_filter.blur_image(roi_image)
        #Third Step Canny the Image
    cannyed_image = i_filter.auto_canny(filtered)
    if config.show_canny :
        cv2.imshow('canny',cannyed_image)
    # 4. Step get Canny Lines with hough algorithm
    lines = cv2.HoughLinesP(
            cannyed_image,
            rho=6,
            theta=np.pi / 60,
            threshold=50,
            lines=np.array([]),
            minLineLength=40,
            maxLineGap=25
    )
    
    # 5. step filter lines
    leftLines = Lines()
    rightLines = Lines()
    for line in lines:
        for x1, y1, x2, y2 in line:
            if x2 == x1:
                x2 = x2 +1
            slope = float((y2 - y1)) / (x2 - x1)  # <-- Calculating the slope.
            if math.fabs(slope) < 0.3:  # <-- Only consider extreme slope
                continue
            angle = math.degrees(math.atan(slope)) # < -- Calculating the angle to the x axis
            if angle <0:
                angle = angle +180
            angle = 180 - angle
            if not((angle >= 30 and angle <= 85) or (angle >= 120 and angle <= 185)) :
                continue
            # Extend the lines to the entire image
            theta = np.arctan2((y2-y1),(x2-x1))
            m2= np.tan(theta)
            c2 = y1 - m2*x1
            x3 = int(1000 + x1) # 1000 was chosen arbitrarily (any number higher than half the image width) 
            y3 = int(m2*x3 + c2)
            x4 = int(x1 - 1000) # 1000 was chosen arbitrarily 
            y4 = int(m2*x4 + c2)
            startPoint = Point(x3,y3)
            endPoint = Point(x4,y4)
            line = Line(startPoint, endPoint)
            if slope <= 0:  # <-- If the slope is negative, left group.
                leftLines.addLine(line)
            else:  # <-- Otherwise, right group.
                #right_lines.extend([line.tolist()])
                rightLines.addLine(line)
    print len(leftLines.getList())
    print len(rightLines.getList())
    if ( len(leftLines.getList()) == 0 and len(rightLines.getList()) == 0):
        print ('beide Arrays leer -> linien konntent nicht erkannt werden')
        if config.only_image_processing == False:   
            fresh = freshness.get()
            #put invalid value
            #print('Waiting......')
            #drivingDone.wait()
            distance.put(None)
            #drivingDone.clear()
            freshness.put(fresh+1)
        return image
    elif  len(leftLines.getList()) == 0:
        #left line empty drive left
        fresh = freshness.get()
        if config.only_image_processing == False:          
            if fresh < 4:
                freshness.put(fresh + 1)
            else:    
                #print('Waiting......')
                #drivingDone.wait()
                distance.put(-101)
                #drivingDone.clear()
        print ('linkes array ist leer -> linien konntent nicht erkannt werden')
        return image
    elif len(rightLines.getList()) == 0:
        #right line empty drive right
        fresh = freshness.get()
        if config.only_image_processing == False:   
            if fresh < 4:
                freshness.put(fresh + 1)
            else:
                #print('Waiting......')
                #drivingDone.wait()
                distance.put(101)
                freshness.put(1)
                #drivingDone.clear()
        print ('rechtes array ist leer -> linien konntent nicht erkannt werden')
        return image
    freshness.put(0)
    #6.Step Find Intersections of left an right Lines
    line_point_dict = vpd.find_intersections(leftLines.getList(),rightLines.getList())
    if config.show_intersection_points:    
        for x,y in line_point_dict.itervalues():
            cv2.circle(image,(int(x) ,int(y)),5,(255,0,0),1)
    grid_size = 80
    #6.Step Filter Lines who have no intersections in Vanishing Point
    lines = vpd.find_vanishing_point(image,grid_size, line_point_dict)
    #set to line Conversion
    left = list(lines[0])
    right = list(lines[1])
    leftLines = Lines()
    rightLines = Lines()
    for line in left:
        newLine = Line(line)
        leftLines.addLine(newLine)
    for line in right:
        newLine = Line(line)
        rightLines.addLine(newLine)
    image = drawLines2(image, leftLines.getList(), color = [0,255,0] )
    image = drawLines2(image, rightLines.getList(), color = [0,0,255])
    rows,cols = image.shape[:2]
    #7.Step calculate average Line with fitLine Algorithm
    abc = leftLines.getArray()
    [vx,vy,x,y] = cv2.fitLine(abc,cv2.DIST_L2,0, 0.01,0.01)
    lefty = int((-x*vy/vx) + y)
    righty = int(((cols-x)*vy/vx)+y)
    left_bottom = (cols-1,righty)
    left_top = (0,lefty)
    cv2.line(image,(cols-1,righty),(0,lefty),(255,255,255),2)
    abc = rightLines.getArray()
    [vx,vy,x,y] = cv2.fitLine(abc,cv2.DIST_L2,0, 0.01,0.01)
    lefty = int((-x*vy/vx) + y)
    righty = int(((cols-x)*vy/vx)+y)
    right_bottom = (cols-1,righty)
    right_top = (0,lefty)
    cv2.line(image,(cols-1,righty),(0,lefty),(255,255,255),2)
    # suche mittelpunkt der strecke bei y = cofig.cut_top
    # y = mx+t
    #y = config.cut_top, m = (y2-y1)/(x2-x1)
    mLeft = float((left_top[1] - left_bottom[1]))/float((left_top[0] - left_bottom[0]))
    # z.B. m = -2 punkt: 58 318
    # y = m * x +t
    #t = y -mx
    #t = 318 - (-2*58) = 434
    tLeft = float(left_top[1] - (mLeft * left_top[0]))
    #x bei y cut_top berechnen
    # cut_top(z.B.250) = mLeft * xLeft + tLeft
    xLeft = float((config.cut_top - tLeft))/float(mLeft)
    mRight = float((right_top[1] - right_bottom[1] ))/float((right_top[0] - right_bottom[0]))
    tRight = float(right_top[1] - (mRight * right_top[0]))
    xRight = float((config.cut_top - tRight))/float(mRight)
    #kreis zeichnen in Bild Mitte
    cv2.circle(image, (image.shape[1] / 2 ,config.cut_top), 5, (0,255,0), thickness = 5)
    if xLeft > 0:
        cv2.circle(image, (int(xLeft), config.cut_top), 5, (0, 0, 255), thickness = 5)
    if xRight > 0:
        cv2.circle(image, (int(xRight), config.cut_top), 5, (0, 0, 255), thickness = 5)
    streckenMitte = xLeft + ((xRight - xLeft) / 2)
    kameraMitte = float(image.shape[1]) / float(2)
    if config.only_image_processing == False:
        print('Waiting......')
        #drivingDone.wait()
    distance.put( streckenMitte - kameraMitte)
    freshness.put(0)
    #drivingDone.clear()
    print('Abweichung:')
    print(streckenMitte - kameraMitte)   
    cv2.circle(image, (int(streckenMitte), config.cut_top), 3, (0, 0, 255), thickness = 5)
    return image
    
