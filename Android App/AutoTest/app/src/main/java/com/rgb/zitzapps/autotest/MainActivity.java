package com.rgb.zitzapps.autotest;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, View.OnTouchListener, View.OnClickListener {

    private BluetoothAdapter bluetoothDefaultAdapter = null;
    private List<String> deviceList = new ArrayList<>();
    private ListView listView;
    private ArrayAdapter adapter;
    private List<BluetoothDevice> btDevices = new ArrayList<>();
    private BluetoothDevice bluetoothDevice = null;
    private BluetoothSocket socket = null;
    private Button front, back, right, left, discover;
    private final BroadcastReceiver bReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String btAction = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(btAction)) {
                // Discovery has found a device. Get the BluetoothDevice
                // object and its info from the Intent.
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                deviceList.add("Name: " + device.getName() + " \nAddress: " + device.getAddress());
                btDevices.add(device);
                adapter.notifyDataSetChanged();
            }
            //BT Device Connected
            if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(btAction))
            {
                Toast.makeText(getApplicationContext(), "Sucessfully Connected", Toast.LENGTH_SHORT).show();
            }
            else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(btAction))
            {
                Toast.makeText(getApplicationContext(), "Device disconnected", Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //bind view elements
        listView = (ListView) findViewById(R.id.listView1);
        front = (Button) findViewById(R.id.btnForward);
        back = (Button) findViewById(R.id.btnBackwards);
        left = (Button) findViewById(R.id.btnLeft);
        right = (Button) findViewById(R.id.btnRight);
        discover = (Button) findViewById(R.id.btnDiscover);
        discover.setOnClickListener(this);
        front.setOnTouchListener(this);
        back.setOnTouchListener(this);
        left.setOnTouchListener(this);
        right.setOnTouchListener(this);
        bluetoothDefaultAdapter = BluetoothAdapter.getDefaultAdapter();
        //check if device supports Bluetooth
        if (bluetoothDefaultAdapter == null) {
            Toast.makeText(this, "Your device does not support Bluetooth", Toast.LENGTH_LONG).show();
        }
        //check if bluetooth is switched on if not show dialog and enable it
        if (!bluetoothDefaultAdapter.isEnabled()) {
            startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), 1);
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 2);
            bluetoothDefaultAdapter.startDiscovery();
        }
        // Register for broadcasts when a device is discovered, connected or disconnected.
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(bReceiver, filter);
        registerReceiver(bReceiver, new IntentFilter(BluetoothDevice.ACTION_ACL_CONNECTED));
        registerReceiver(bReceiver, new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(bReceiver);
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, deviceList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 2);
            //bluetooth activated now search for devices
            bluetoothDefaultAdapter.startDiscovery();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d("Main", "Item clicked: " + position);
        bluetoothDevice = (BluetoothDevice) btDevices.toArray()[position];
        try {
            //If you are connecting to a Bluetooth serial board then try
            //using the well-known SPP UUID 00001101-0000-1000-8000-00805F9B34FB.
            socket = bluetoothDevice.createInsecureRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
            if (!socket.isConnected()) {
                if (bluetoothDefaultAdapter.isDiscovering()) {
                    //needs huge bandwidth cancel before connecting
                    bluetoothDefaultAdapter.cancelDiscovery();
                }
                socket.connect();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void sendCommand(int a) {
        try {
            if (socket == null) {
                Toast.makeText(this, "No device selected", Toast.LENGTH_SHORT).show();
                return;
            }

            if (!socket.isConnected()) {
                Toast.makeText(this, "Device not connected", Toast.LENGTH_SHORT).show();
                return;
            }
            Log.d("Sending commend", "Command:" + a);
            socket.getOutputStream().write(new byte[]{(byte) a}, 0, 1);
            socket.getOutputStream().flush();
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.btnForward:
                if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_MOVE) {
                    sendCommand(AutoCommands.Forward_Go);
                } else
                    sendCommand(AutoCommands.Forward_Stop);
                break;
            case R.id.btnBackwards:
                if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_MOVE) {
                    sendCommand(AutoCommands.Backwards_Go);
                } else
                    sendCommand(AutoCommands.Backwards_Stop);
                break;
            case R.id.btnRight:
                if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_MOVE) {
                    sendCommand(AutoCommands.Right_Go);
                } else
                    sendCommand(AutoCommands.Right_Stop);
                break;
            case R.id.btnLeft:
                if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_MOVE) {
                    sendCommand(AutoCommands.Left_Go);
                } else
                    sendCommand(AutoCommands.Left_Stop);
                break;
        }
        return false;
    }

    //discover Bt devices
    @Override
    public void onClick(View v) {
        if(!bluetoothDefaultAdapter.isDiscovering()){
            btDevices.clear();
            deviceList.clear();
            adapter.notifyDataSetChanged();
            bluetoothDefaultAdapter.startDiscovery();
        }
    }
}
