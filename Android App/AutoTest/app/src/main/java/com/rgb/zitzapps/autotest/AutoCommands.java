package com.rgb.zitzapps.autotest;

/**
 * Created by philipp on 21.03.2018.
 */

public class AutoCommands
{
    static final int Forward_Stop = 0;
    static final int Forward_Go = 1;
    static final int Backwards_Stop = 2;
    static final int Backwards_Go = 3;
    static final int Left_Stop = 4;
    static final int Left_Go = 5;
    static final int Right_Stop = 6;
    static final int Right_Go = 7;
}
