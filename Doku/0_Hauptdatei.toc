\contentsline {section}{Abk\IeC {\"u}rzungsverzeichnis}{5}{section*.3}
\contentsline {section}{\numberline {1}Einleitung}{6}{section.1}
\contentsline {subsection}{\numberline {1.1}Aktuelle Situation}{6}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Anforderungen}{7}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Zielsetzung}{7}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Allgemeiner Projektaufbau}{7}{subsection.1.4}
\contentsline {section}{\numberline {2}Verwendete Hardware}{8}{section.2}
\contentsline {subsection}{\numberline {2.1}BeeWi Bluetooth Car}{8}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Hama Black Tube HD-Webcam}{9}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Raspberry Pi 3}{10}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Technische Daten}{10}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}Betriebssystem}{12}{subsubsection.2.3.2}
\contentsline {subsubsection}{\numberline {2.3.3}Steuerung des Raspberry Pi per Remote Desktop}{12}{subsubsection.2.3.3}
\contentsline {section}{\numberline {3}Implementierung einer Android Applikation zur Steuerung des Autos}{14}{section.3}
\contentsline {subsection}{\numberline {3.1}Test der Steuerungsapp aus dem App Store}{14}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Herausfinden der Bluetooth Commands}{15}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Implementierung der Android App}{16}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}Layout}{16}{subsubsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.2}Suchen von Ger\IeC {\"a}ten in der N\IeC {\"a}he}{17}{subsubsection.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.3}Herstellung der Verbindung}{19}{subsubsection.3.3.3}
\contentsline {section}{\numberline {4}Erkennung der Fahrspur mit Hilfe von Computer Vision}{22}{section.4}
\contentsline {subsection}{\numberline {4.1}OpenCV (Open Source Computer Vision Library)}{22}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Python}{24}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Vorteile}{24}{subsubsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.2}Nachteil}{25}{subsubsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.3}Versionen}{25}{subsubsection.4.2.3}
\contentsline {subsubsection}{\numberline {4.2.4}Installation}{25}{subsubsection.4.2.4}
\contentsline {subsection}{\numberline {4.3}Versuchsaufbau}{26}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Spurerkennung in Realbedingungen}{27}{subsection.4.4}
\contentsline {subsubsection}{\numberline {4.4.1}Line estimation Method}{27}{subsubsection.4.4.1}
\contentsline {subsubsection}{\numberline {4.4.2}Vanishing point estimation}{28}{subsubsection.4.4.2}
\contentsline {subsubsection}{\numberline {4.4.3}Lane detection Method}{29}{subsubsection.4.4.3}
\contentsline {subsection}{\numberline {4.5}Implementierung des Algorithmus zur Erkennung der Fahrspur}{30}{subsection.4.5}
\contentsline {subsubsection}{\numberline {4.5.1}Ausf\IeC {\"u}hrung des Projektes}{30}{subsubsection.4.5.1}
\contentsline {subsubsection}{\numberline {4.5.2}Region of Interest}{30}{subsubsection.4.5.2}
\contentsline {subsubsection}{\numberline {4.5.3}Canny Edge Detection}{32}{subsubsection.4.5.3}
\contentsline {subsubsection}{\numberline {4.5.4}Hough Line Algorithmus}{35}{subsubsection.4.5.4}
\contentsline {subsubsection}{\numberline {4.5.5}Detektion der Fahrspur}{36}{subsubsection.4.5.5}
\contentsline {subsubsection}{\numberline {4.5.6}Berechnung der Abweichung des aktuellen Kurses zur Fahrbahnmitte}{43}{subsubsection.4.5.6}
\contentsline {subsubsection}{\numberline {4.5.7}Anwenden von Filtern zur besseren Linienerkennung}{44}{subsubsection.4.5.7}
\contentsline {section}{\numberline {5}Implementierung der Steuerung des Autos}{45}{section.5}
\contentsline {subsection}{\numberline {5.1}Bluetooth Connection}{45}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Image Thread}{46}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Image Processing Thread}{46}{subsection.5.3}
\contentsline {section}{\numberline {6}Ausblick}{47}{section.6}
\contentsline {section}{\numberline {7}Fazit}{47}{section.7}
\contentsline {section}{Literaturverzeichnis}{48}{section.7}
\contentsline {section}{Verzeichnis der Webadressen}{49}{section*.37}
\contentsline {section}{Abbildungsverzeichnis}{50}{section*.38}
\contentsline {section}{Tabellenverzeichnis}{51}{section*.39}
\contentsline {section}{Listingverzeichnis}{52}{section*.40}
\contentsline {section}{Anhang}{53}{section*.41}
